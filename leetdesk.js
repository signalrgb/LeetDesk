/* eslint-disable */
export function Name() { return "LeetDesk"; }
export function Version() { return "1.0.0"; }
export function Type() { return "network"; }
export function Publisher() { return "LeetDesk"; }
export function Documentation() { return ""; }
export function Size() { return [1, 1]; }
export function DefaultPosition(){return [0, 0]; }
export function DefaultScale(){return 1.0; }
export function SubdeviceController(){ return true; }
export function DefaultComponentBrand() { return "LeetDesk"; }
export function ControllableParameters() {
	return [
		{"property":"LightingMode", "group":"settings", "label":"Lighting Mode", "type":"combobox", "values":["Canvas", "Forced"], "default":"Canvas"},
		{"property":"forcedColor", "group":"settings", "label":"Forced Color", "min":"0", "max":"360", "type":"color", "default":"#009bde"},
		{"property":"turnOffOnShutdown", "group":"settings", "label":"Turn WLED device OFF on Shutdown", "type":"boolean", "default":"false"},
	];
}

let WLED;
const MaxLedsInPacket = 485;
const BIG_ENDIAN = 1;
const colorBlack = "#000000";
let lastForcedUpdate = 0;

class WLEDDevice {
	constructor(controller) {
		this.mac = controller.mac;
		this.hostname = controller.hostname;
		this.name = controller.name;
		this.ip = controller.ip;
		this.port = controller.port;
		this.streamingPort = controller.streamingPort;
		this.deviceledcount = controller.deviceledcount;
		this.defaultOn = controller.defaultOn;
		this.defaultBri = controller.defaultBri;
	}

	changeDeviceState(forceOff = false, forceOn = false, fullBright = false) {
		DeviceState.Change(this.ip, this.defaultOn, this.defaultBri, forceOff, forceOn, fullBright, false);
	}

	SetupChannel() {
		device.SetLedLimit(this.deviceledcount);
		device.addChannel(this.name, this.deviceledcount);
	}

	SendColorPackets(shutdown = false) {
		const componentChannel = device.channel(this.name);
		let ChannelLedCount = componentChannel.ledCount > this.deviceledcount ? this.deviceledcount : componentChannel.ledCount;

		let RGBData = [];

		if(shutdown) {
			RGBData = device.createColorArray(colorBlack, ChannelLedCount, "Inline");
		} else if(LightingMode === "Forced") {
			RGBData = device.createColorArray(forcedColor, ChannelLedCount, "Inline");
		} else if(componentChannel.shouldPulseColors()) {
			ChannelLedCount = this.deviceledcount;

			const pulseColor = device.getChannelPulseColor(this.name);
			RGBData = device.createColorArray(pulseColor, ChannelLedCount, "Inline");
		} else {
			RGBData = componentChannel.getColors("Inline");
		}

		const NumPackets = Math.ceil(ChannelLedCount / MaxLedsInPacket);

		for(let CurrPacket = 0; CurrPacket < NumPackets; CurrPacket++) {
			const startIdx = CurrPacket * MaxLedsInPacket;
			const highByte = ((startIdx >> 8) & 0xFF);
			const lowByte = (startIdx & 0xFF);
			let packet = [0x04, 0x02, highByte, lowByte];
			packet = packet.concat(RGBData.splice(0, MaxLedsInPacket*3));
			udp.send(this.ip, this.streamingPort, packet, BIG_ENDIAN);
		}
	}
}

export function Initialize() {
	device.setName(controller.name);
	device.setImageFromUrl("https://assets.signalrgb.com/brands/leetdesk/logo.jpg");
	device.addFeature("udp");
	WLED = new WLEDDevice(controller);
	WLED.SetupChannel();
	WLED.changeDeviceState(false, true, true);
}

export function Render() {
	WLED.SendColorPackets();
}

export function Shutdown(suspend) {
	WLED.SendColorPackets(true);
	WLED.changeDeviceState(turnOffOnShutdown);
}

// -------------------------------------------<( Discovery Service )>--------------------------------------------------


export function DiscoveryService() {
	this.IconUrl = "https://assets.signalrgb.com/brands/leetdesk/logo.jpg";

	this.MDns = [ "_aura._tcp.local." ];					//_wled._tcp.local.

	this.forceDiscover = function(ipaddress) {
		if(!ipaddress || ipaddress === undefined) {

		} else if (this.isValidIP(ipaddress)) {
			service.log("Forcing Discovery for Aura device at IP: " + ipaddress);
			this.ip = ipaddress;
			this.port = 80;
			this.forced = true;
			this.offline = false;
			this.prepareDiscovery(false);

		} else {

		}
	};

	this.forceDelete = function(ipaddress) {
		if(!ipaddress || ipaddress === undefined) {

		} else if (this.isValidIP(ipaddress)) {
			let devicelist_string = service.getSetting("forcedDiscovery", "devicelist");

			if(devicelist_string === undefined || devicelist_string.length === 0) {

			} else if(devicelist_string !== undefined && devicelist_string.length > 0 && devicelist_string.includes(ipaddress)) {
				service.log("Force Deleting Aura device at IP: " + ipaddress);

				const devicelist = JSON.parse(devicelist_string);
				const macAddress = devicelist[ipaddress];
				delete devicelist[ipaddress];
				devicelist_string = JSON.stringify(devicelist);
				service.saveSetting("forcedDiscovery", "devicelist", devicelist_string);
				this.ip = ipaddress;
				this.mac = macAddress;
				this.port = 80;
				this.forced = true;
				this.prepareDiscovery(true);

			}
		} else {

		}
	};

	this.isValidIP = function(ipaddress) {
		if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
			return true;
		}

		return false;
	};

	this.prepareDiscovery = function(deletion = false) {
		const instance = this;

		if(!deletion) {
			service.log(`Requesting basic Device Information for forced discovery...`);
			service.log(`http://${instance.ip}:${instance.port}/jsonaura/info/`);
			XmlHttp.Get(`http://${instance.ip}:${instance.port}/jsonaura/info/`, (xhr) => {

				if (xhr.readyState === 4) {
					if(xhr.status === 200) {
						const devicedata = JSON.parse(xhr.response);
						const wledname = devicedata.name === "aura" ? "aura-" + devicedata.mac.substr(devicedata.mac.length - 6) : devicedata.name;  // WLED weld
						const forcedvalue = {"hostname":devicedata.ip, "mac":devicedata.mac, "name":wledname, "port":80, "forced":true};
						instance.Discovered(forcedvalue);
					} else {
						service.log("ERROR for IP " + instance.ip + ", device is OFFLINE or does not respond!");
					}
				}
			});
		} else {
			for(const controller of service.controllers) {
				if(controller.id === instance.mac) {
					service.removeSetting(controller.id, "name");
					service.removeSetting(controller.id, "ip");
					service.removeController(controller);

					return;
				}
			}
		}
	};

	this.Initialize = function() {
		service.log("Initializing Plugin!");
		service.log("Searching for network devices...");
	};

	this.loadForcedDevices = function() {
		const devicelist_string = service.getSetting("forcedDiscovery", "devicelist");

		if(devicelist_string !== undefined && devicelist_string.length > 0) {
			service.log("Refreshing force discovered devices...");

			const devicelist = JSON.parse(devicelist_string);
			Object.keys(devicelist).forEach(key => {
				let controllerExists = false;

				for(const cont of service.controllers) {
					if(cont.obj.ip === key) {
						controllerExists = true;
					}
				}

				if(!controllerExists) {
					this.forceDiscover(key);
				}
			});
		}
	};

	this.Update = function() {
		for(const cont of service.controllers) {
			cont.obj.update();
		}
		const currentTime = Date.now();

		if(currentTime - lastForcedUpdate >= 60000) {
			lastForcedUpdate = currentTime;
			this.loadForcedDevices();
		}
	};

	this.Discovered = function(value) {
		service.log("Device discovered:");
		service.log(value);

		const controller = service.getController(value.mac);

		if (controller === undefined) {
			service.addController(new WLEDBridge(value));
		} else {
			if(this.forced === value.forced) {
				controller.updateWithValue(value);
			}
		}
	};
}

class WLEDBridge {
	constructor(value) {
		this.readyToAnnounce = false;
		this.announced = false;
		this.hostname = value.hostname;
		this.mac = value.mac;
		this.name = value.name;
		this.id = value.mac;
		this.port = value.port;
		this.arch = "";
		this.deviceledcount = 512;
		this.firmwareversion = 0;
		this.linked = service.getSetting(this.mac, "ip");
		this.ip = "";
		this.connected = false;
		this.forced = value.forced;
		this.defaultOn = false;
		this.defaultBri = 128;
		this.signalstrength = 0;
		this.lastUpdate = Date.now();
		this.firstUpdate = true;
		this.offline = false;

		service.log("Constructed: "+this.name);

		if(!this.forced) {
			this.getDeviceIP();
		} else {
			this.ip = this.hostname;
			this.getDeviceInfo();
		}
	}

	updateWithValue(value) {
		this.forced = value.forced;
		this.hostname = value.hostname;
		this.mac = value.mac;
		this.name = value.name;
		this.port = value.port;
		this.id = value.mac;
		this.ip = this.forced ? value.hostname : value.ip;

		if(this.forced) {
			this.saveForceDiscovery();
		}

		service.log("Updated: "+this.mac);
		service.updateController(this);
		this.getDeviceInfo();
	}

	update() {
		if (this.waitingforlink){
			this.waitingforlink = false;
			this.connected = this.linked === this.ip;
			this.readyToAnnounce = true;
			service.updateController(this);
		}

		this.createDevice();

		const currentTime = Date.now();

		if(currentTime - this.lastUpdate >= (Math.floor(Math.random() * 10000) + 50000)) {
			this.lastUpdate = currentTime;
			this.getDeviceInfo();
		}
	}

	createDevice() {
		if(this.readyToAnnounce && !this.announced) {
			if(!this.connected) {
				this.saveController();
				service.log("Adding Controller: " + this.name + " - IP: " + this.ip + " - UDP Port: " + this.streamingPort);
			} else {
				service.updateController(this);
				service.announceController(this);
				service.log("Announcing existing Controller: " + this.name + " - IP: " + this.ip + " - UDP Port: " + this.streamingPort);
			}

			this.announced = true;

			if(this.connected) {
				DeviceState.Change(this.ip, this.defaultOn, this.defaultBri, false, true, true);
			}

			service.updateController(this);
		}
	}

	saveController() {
		service.saveSetting(this.mac, "name", this.name);
		service.saveSetting(this.mac, "ip", this.ip);
		service.updateController(this);
		service.announceController(this);
		this.connected = true;
	}

	getDeviceInfo() {
		const instance = this;
		service.log(`Requesting complete Device Information...`);
		service.log(`http://${instance.ip}:${instance.port}/jsonaura/`);
		XmlHttp.Get(`http://${instance.ip}:${instance.port}/jsonaura/`, (xhr) => {

			if (xhr.readyState === 4) {
				if(xhr.status === 200) {
					instance.offline = false;
					service.updateController(instance);
					instance.setDeviceInfo(JSON.parse(xhr.response));
				} else {
					instance.offline = true;
					service.updateController(instance);
					service.log("ERROR for IP " + instance.ip + ", device is OFFLINE or does not respond!");
				}
			}
		});
	}

	getDeviceIP() {
		const instance = this;
		service.log(`Reading IPV4 from JSON API...`);

		const mdnsHostname = instance.hostname.substring(0, instance.hostname.length - 1);
		service.log(`http://${mdnsHostname}:${this.port}/jsonaura/`);
		XmlHttp.Get(`http://${mdnsHostname}:${this.port}/jsonaura/`, (xhr) => {

			if (xhr.readyState === 4) {
				if(xhr.status === 200) {
					const devicedata = JSON.parse(xhr.response);
					instance.ip = devicedata.info.ip;
					service.log("IP for " + instance.hostname + " received: " + instance.ip);
					instance.offline = false;
					service.updateController(instance);
					instance.setDeviceInfo(devicedata);
				} else {
					instance.offline = true;
					service.updateController(instance);
					service.log("ERROR for mdnsHostname: " + instance.hostname + ", device is OFFLINE or does not respond!");
				}
			}
		});
	}

	setDeviceInfo(response) {
		if(this.firstUpdate) {
			this.defaultOn = response.state.on;
			this.defaultBri = response.state.bri;
			this.firstUpdate = false;
		}

		this.streamingPort = response.info.udpport;
		this.arch = response.info.arch;
		this.firmwareversion = response.info.ver;
		this.deviceledcount = response.info.leds.count;
		this.signalstrength = response.info.wifi.signal;
		service.log("Device info for " + this.name + " with IP " + this.ip + " received:");
		service.log("UDP Port: " + this.streamingPort + " - Arch: " + this.arch + " - Firmware: " + this.firmwareversion + " - LED count: " + this.deviceledcount + " - default State: " + (this.defaultOn ? "ON" : "OFF") + " - default Brightness: " + this.defaultBri + " - Signal strength: " + this.signalstrength);

		this.linked = service.getSetting(this.mac, "ip");

		if(this.linked === this.ip) {
			this.connected = true;
			this.readyToAnnounce = true;
		}

		if(this.forced) {
			this.saveForceDiscovery();
		}

		service.updateController(this);
	}

	saveForceDiscovery() {
		let devicelist_string = service.getSetting("forcedDiscovery", "devicelist");
		let devicelist = {};

		if(devicelist_string === undefined || devicelist_string.length === 0) {
			devicelist[this.ip] = this.mac;
			devicelist_string = JSON.stringify(devicelist);
			service.saveSetting("forcedDiscovery", "devicelist", devicelist_string);
		} else if(devicelist_string !== undefined && devicelist_string.length > 0 && !devicelist_string.includes(this.ip)) {
			devicelist = JSON.parse(devicelist_string);
			devicelist[this.ip] = this.mac;
			devicelist_string = JSON.stringify(devicelist);
			service.saveSetting("forcedDiscovery", "devicelist", devicelist_string);
		}
	}

	startLink() {
		DeviceState.Change(this.ip, this.defaultOn, this.defaultBri, false, true, true);
		this.waitingforlink = true;
		service.updateController(this);
	}

	startRemove() {
		DeviceState.Change(this.ip, this.defaultOn, this.defaultBri);
		service.removeSetting(this.mac, "name");
		service.removeSetting(this.mac, "ip");
		this.connected = false;
		this.announced = false;
		this.readyToAnnounce = false;
		service.suppressController(this);
		service.updateController(this);
	}

	startDelete() {
		discovery.forceDelete(this.ip);
	}

	startForceDiscover() {
		discovery.forceDiscover(this.ip);
	}

	turnOn() {
		DeviceState.Change(this.ip, this.defaultOn, this.defaultBri, false, true, true);
	}

	turnOff() {
		DeviceState.Change(this.ip, this.defaultOn, this.defaultBri, true);
	}
}

class XmlHttp {
	static Get(url, callback, async = true) {
		const xhr = new XMLHttpRequest();
		xhr.open("GET", url, async);

		//xhr.timeout = 2000;
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");

		xhr.onreadystatechange = callback.bind(null, xhr);

		xhr.send();
	}

	static Post(url, callback, data, async = true) {
		const xhr = new XMLHttpRequest();
		xhr.open("POST", url, async);

		//xhr.timeout = 2000;
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");

		xhr.onreadystatechange = callback.bind(null, xhr);

		xhr.send(JSON.stringify(data));
	}
}

class DeviceState {
	static Change(ip, defaultOn, defaultBri, forceOff = false, forceOn = false, fullBright = false, async = true) {
		const JSONurl = "http://" + ip + ":80/jsonaura/state/";
		XmlHttp.Post(JSONurl, (xhr) => {
			if(xhr.readyState === 4 && xhr.status === 200) {
				// request successful, do some shit later on here!
			}
		},
		{on: (forceOff ? false : forceOn ? true : defaultOn), bri: (fullBright ? 255 : defaultBri), live: false},
		async);
	}
}


export function Image(){
	return "iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAACXBIWXMAAAsTAAALEwEAmpwYAAAIh2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgNzkuMTY0NDg4LCAyMDIwLzA3LzEwLTIyOjA2OjUzICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIyLjAgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wOC0wOVQxMzowNjowOCswMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMy0wOC0wOVQxMzowNzo1NSswMjowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDgtMDlUMTM6MDc6NTUrMDI6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjViY2M3MDQ1LWU3NzItODU0My05MmVhLTM2YTYwMTQwMWNhZSIgeG1wTU06RG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjJkZmRkOGQyLTYzMTUtN2I0YS1hMzljLTc0OWE1MzFmODA0YiIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjFmMmFmZGZhLTY2OTYtZWQ0NC04Y2ZmLTAwNjRlNjQxOGEyYSIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6MWYyYWZkZmEtNjY5Ni1lZDQ0LThjZmYtMDA2NGU2NDE4YTJhIiBzdEV2dDp3aGVuPSIyMDIzLTA4LTA5VDEzOjA2OjA4KzAyOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjIuMCAoV2luZG93cykiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjQwZDM1OGIzLWJlM2EtZDc0ZS1hYjFjLTBhZGVhNTc4MTNjNiIgc3RFdnQ6d2hlbj0iMjAyMy0wOC0wOVQxMzowNzo1NSswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIyLjAgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjb252ZXJ0ZWQiIHN0RXZ0OnBhcmFtZXRlcnM9ImZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImRlcml2ZWQiIHN0RXZ0OnBhcmFtZXRlcnM9ImNvbnZlcnRlZCBmcm9tIGFwcGxpY2F0aW9uL3ZuZC5hZG9iZS5waG90b3Nob3AgdG8gaW1hZ2UvcG5nIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo1YmNjNzA0NS1lNzcyLTg1NDMtOTJlYS0zNmE2MDE0MDFjYWUiIHN0RXZ0OndoZW49IjIwMjMtMDgtMDlUMTM6MDc6NTUrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMi4wIChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDBkMzU4YjMtYmUzYS1kNzRlLWFiMWMtMGFkZWE1NzgxM2M2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjFmMmFmZGZhLTY2OTYtZWQ0NC04Y2ZmLTAwNjRlNjQxOGEyYSIgc3RSZWY6b3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjFmMmFmZGZhLTY2OTYtZWQ0NC04Y2ZmLTAwNjRlNjQxOGEyYSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PhSc8BYAAD8nSURBVHic7d13eFb1/f/x152EDBJIAklYASJLlB0RkIos0QIypFAHP5wtFXFgrVWrX9GKtK6Cu1gHIlpFRIQyBBQEGSobBGSGvcJICGRzfn9QrCO5z7nv3POT5+O6znW13p9zzjsh9/26zzmf4bIsSwAAILxFBLsAAABQcQQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYICoiuzscrncvRwnaYCkPpLaS2ooqWpFzgcAQJg6I2m3pJWSZkv6VFJ+eY29WdrcVZH10MsJ9ChJwyX9n6TaXh8cAABzHZL0pKTXJZX8/MVQCPQLJE2X1NrrgwIAUHmslzRQ0q4f/0dvstmXz9B/JekbEeYAADjVWuey81cVPZCvrtAvlrRMUmJFCwIAoBLK0blQ/04K3i33GEmrJLXw+kAAAGCTpExJhcG65T5KhDkAABV1sc5lqlcqeoUeJ2mPpBSvDwIAAM7LltTQsqwznu5Y0Sv0ASLMAQDwlRRJ/b3ZsaKB3reC+wMAgJ/yKlsrGuiZFdwfAAD8lFfZWtFn6KfFdK4AAPjSGcuy4j3dqaKB7v3OAACgTJZluV0spSystgYAgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMEBUsAsIlLS0NN1www3q2rWrmjdvrpo1ayomJibYZQEBc+zYMR06dEhff/21pk+friVLlsiyrKDW1LBhQ/Xv31/9+/dXr169gloLEO5cFXlDu1yu4H4aOJCYmKi//vWvGjFihKpUqRLscoCQsWHDBo0aNUpffPFFwM7pcrnUvn37H0K8devWP3kNwDmWZXn8hjA60Js0aaI5c+aoSZMmwS4FCFmPP/64nnjiCb8dPy4uTj179lT//v3Vr18/1a5du8x2BDq8FR8frxYtWuibb74Jdik+Q6D/SGpqqlavXq309PRglwKEvD/96U96/vnn/XLsrVu3qmnTprbtCHR4ok6dOurXr5/69++vnj176uuvv1a3bt2CXZbPeBPoxj5Df/rppwlzwKGxY8dq2rRp2rVrl8+PHR0d7fNjonJq0aKFBgwYoAEDBujSSy/lS+DPGBno8fHxuuGGG4JdBhA2oqOjdcstt2j06NHBLgX4hXHjxqlfv35q3LhxsEsJaUYGeufOnRUbG1vu61OnTtUrr7wSwIqA4KtWrZpmzJhR7uvdu3cn0BGSRo0aFewSwoKRgX7ppZe6fX3OnDlatGhRYIoBQsiBAwdUt27dMl+75JJLFBkZqdLS0gBXBcAXjJxYpmPHjm5fX7lyZYAqAULLmjVryn2tatWqatGiRQCrAeBLRgZ6hw4dyn2tqKhImzdvDmA1QOhYvXq129ftvgwDCF3GBXr9+vXLHecqSevXr1dxcXEAKwJCh93dKXdfhgGENuMC3e4KY9WqVQGqBAg97m65SwQ6EM6MC3S7DyS7DzTAZHv37lV2dna5r7ds2VLx8fEBrAiAr1S6QLd7hgiYzt1dqoiICGVmZgawGgC+YtSwtcjISLVv377c10tKSrR+/foAVgSEnjVr1ujqq68u9/VOnTppyZIlAazIfA0aNFCjRo1Uv3591axZU/Hx8apSpYosy1JOTo5ycnK0f/9+bdu2zS+z9XkqOjpaF110kdLT01W/fn3FxcUpMTFRklRQUKDTp0/r8OHD2rNnjzZv3qycnJwgVwzJsEC/6KKL3N4u/O6771RYWBjAioDQY3eXym4eB9irW7euBgwYoL59+6pjx45KSUlxvG9ubq6WLl2quXPnaurUqTpw4IAfKz3H5XLpsssu04ABA9SjRw+1bt3aoyl7t2/fri+++EKzZs3SnDlzvOp4nJmZqXvuucfj/c5r2bKlpk+f7rj9TTfdpNzcXK/PF5Isy/J6k2SF0va73/3Ocuett94Keo1sbMHeGjVq5PZ9snv3bp+eLysry+35zgv278UXW+fOna2PP/7YKikpcfQz2ykpKbGmT59uZWZm+qXemJgY66677rK+//57n9RrWZZ16NAha/To0Va1atU8qmXgwIE+q8GJqKiooP+9uNssbzLZm52sEA30CRMmuP0HHDlyZNBrZGMLhe3kyZNu3yu1atXy2bkqQ6Cnp6dbH374oaOf01uTJk2ykpKSfFZzv379rJ07d/qt3uzsbGvo0KGO6wlkoGdnZwf9b8Zus7zIZKM6xdkNWaOHO3CO3W13hq8517t3b61du1a//e1v/XqeYcOGaePGjWrXrl2FjhMdHa2XX35ZM2bM0AUXXOCj6n6pZs2amjx5siZPnhxyK+65G+kRzowJ9KpVq6ply5blvn727FmtW7cugBUBoctuPgZmjHPmpptu0n/+8x/VrFkzIOerV6+eFi9erK5du3q1f9WqVTVnzhyNHDnSx5WVb+jQoZo7d66qVq0asHPaOXbsWLBL8AtjOsVlZmYqMjKy3Ne3bNmi06dPB6yeCy+8UCNGjHDc/sCBA3rmmWf8WBGCITM9Vl0uqKrUapEqLrGUdaJYC7ef0Z4TwZ2tkAlmKu6GG27QO++8E/DzJiQkaMaMGerSpYtHo3YiIyM1ffp09ejRw4/Vla179+768MMPNXDgwJBY/Ofw4cPBLsEvjAl0uyuKQI8/79y5s+69917H7efPn0+gG6RBchVNGFJHnTPifvGaZUmfbjylB/9zRIdOlQShOme33F0u1/m+MviZzMxMvfnmm17tW1hYqKNHj6q0tFQRERGqXr36D0PCnKpevbo+/vhjtWvXTnl5eY72ef7559WrVy9vStbJkyd/GJoWGxurxMREt0tUl+Waa67Rww8/rDFjxnhVgy8dPXo02CX4hTG33O2G2gT6+XmTJk08ar93714/VYJAqxodoU9vSy8zzCXJ5ZIGtqqmhSMbKqNGlQBXd87WrVvdBkFiYqKaNWsWwIrCR0xMjCZNmqS4uLL/fX9u27ZtGjt2rK688krVrl1bsbGxql+/vjIyMtSgQQMlJSUpNjZWHTp00MiRIzV//nxHx23SpInGjh3rqG3Pnj0dX2CUlpZqzpw5GjFihNq1a6e4uDglJycrIyNDGRkZql27tuLi4pSWlqbevXvrqaeecvz59fjjj6tVq1aO2vqTqc/QjenlvmvXLre9Gq+44oqA1uNpj9fRo0cH/XfI5pvt952SrJyxFzraFo1saLlcwalzyZIlbv8mhw0b5pPzmNbL/ZFHHnH08+zdu9e69tprLZfL5fE5LrnkEmvt2rW25ygpKbGaN2/u9ljR0dGOe7NPmzbNatKkicf1RkVFWXfffbeVn59ve4558+aVeYwqVapYSUlJZW5OLFq0KOh/G77crMrayz01NVUZGRlu23CFjkBpW8/5rch29WLVsYGzKz1fs3tP0DHul5KSkvTAAw/Ytlu1apUyMzP1ySefePXYYtWqVercubO+/PJLt+0iIyN1//33u21z2223OerN/sgjj2jQoEHavn27R7VK52bhfOmll9S9e3fl5+e7bdurV68ypxcuLi7WyZMny9zgjBGBbteBZ/v27Tp16lSAqjnH00Dfs2ePnypBoJ0pOutR+/b1PXsW6Ssspeq54cOH2z7vPnTokPr27Vvh57RnzpzR4MGDdeTIEbfthg4dqoSEhDJfc7lcjr6AvP76645v37uzYsUKR7O9DR8+vMLnwi8ZEeihtmRqzZo1Vb16dY/24QrdHKv2FXjUPiYqOG9Du45xbdq0UUxMTICqCQ+33XabbZu7777bZ72os7Oz9dRTT7ltExcXpz59+pT5WteuXdWoUSO3++/fv1/33Xef1zX+3JtvvqmNGze6bTNo0CC3o5LgHSMC3e5KItCB3rRpU4/32b17tx8qQTB8uvGUR8PSvt7j/halv2zZskUFBeV/+YiOjlbbtm0DV1CIy8zM1IUXXui2zdatW/Xxxx/79LzvvPOOSkrcj4YobyjaDTfcYHv8cePG6cyZM17VVhbLsvT222+7bZOamqoWLVr47Jw4p1IE+tq1awNTyH95GujZ2dluP1gRXvKLLV3/7n4dzbMfbzt7c56+2um7D1NPlJSU2E62xEIt/3PVVVfZtvnwww99PtQvJyfH64mAnNQ8ZcoUr+pyZ8GCBbZt6KPhe2Ef6E2bNlVycrLbNoG+Qre7xfVzPD83z3eHCtXl5SxNWZurkrO//IAvLrX02rITuvXf/l9Jyx272+6dOnUKUCWhr3v37rZtli1b5pdz23VUK+si4vwwM3d2797tl8d9O3bssG3DsEjfC/uJZeyuznft2qXjx49X6Bzjx49XUlKS4/aXXXaZR8evXbu2Jk6c6Lj9iRMnfPrMC565JD1WHRrEKSkuUqWWpQM5JfpyxxntPfnT2+wHc0v0+ykH9dCsI+qcEac61avIJengqRIt3XVGx06XfQWfHBepXhfGq2lKtKIipBP5Z7X+YIGW7spXcalvr/7serpzhf4/Th4/fP/99345t91nWHx8vFJSUn4yvtrJnO9bt26tcG1lOX36tAoLC932wWjYsKFfzl2ZGR/oFb3dXrVqVd1zzz1yuVwVOo47devW1c033+y4vV2HE/jHhWnR+ufgOspM/2WvdMuSZn53Sg/OOqIDOT993nnsdKlmfudsNq+7Lk/WI1emqGr0L2+eHT5Voqe/OKa3vjkpX93Vtbt71axZMyUnJ+vEiRO+OWGYSktLU1pamm27Q4cO+eX8Ti5KEhISfhLorVu3tt3Hrgd9RRQUFLgN9GrVqvnt3JVV2N9y93cP9+bNm/s1zL1x8ODBYJdQ6TRIrqI5v29QZphL52Z/69+ymr4c2VAXpnm3stSIzsl6qk9amWEuSbWqRekfA2rpjd/WUaSP3rkbNmxQcbH7DnxcpTu/mrQbg+1PPx9ZU69ePdt9fNkZzlNOZ9qDc2F9hV6lShXb22AVDXR3K7gFi6nzEIeyB3vUVM14+2E2aQlRevfGeur8YlaZz87LExXh0l+uTHHUdnCb6tp6tEhPf1HxFaOKi4ttl+Ts0KGD5s2bV+FzhbM6deo4apeVleWX8zt55BcR8dNvefXr17fd5/rrr3fUcc4bng7dRcWFdaC3bdvWdpxsRW+52w1TCYYDB4LbkaoyuvwC50s/XpgWrZ5Nq+qz752v7tcsNVrVY51fdo+6ooZeXHJC+cWeTWJTllWrVrkNdHojSzVq1HDULpSeCztZ8KVatWrc+jZIWN9yt7sVeODAgQo/0wrFsZJcoQdeTJRnj10u9XA618ISzx6KV42O0KUNfDPDnN1dLGaMk8cri4WCUFp/HIER1oEeiBniQjHQeYYeeJsPF3rUPq6KZ2+tnceLlF1Or/fyJHpwRe+O3V2stLS0kLryDIZwDHRUPmEd6HZXDhVdkCU2NtbjMeWB4M+eqSjbu6tyPGq/Zr9nEwVZljRhuWc9yXcecz4bnTvr1q1Taan7LxOV/bY7Ez8hHIRtoFevXt32+XZFr9CbNWv2i44moYBn6IE3bf0pTVmb66jttuwizdjo+WJA/1h0XJ9vc/bcfemufH13yLO7BuXJz8/Xpk2b3Lap7D3dwzHQT5923ocDZgi9tHKoQ4cOtsPJKhroF198cYX29xeu0IPjjqkH9fyiY257r288WKhBb+9TkRcTwJSctXT9pP16Yclxlbrp67Zyb4Fu8fEMc3a33Sv7jHE5OZ7doQkFgV5hEsEXtr3c7a4Yjh49qv3791foHImJibZzXf9YVFSUx8/cDx8+7HHHPTrFBUfpWemv87L1xtcnNaR1dV3aIFbJVSNVelbae7JYn31/WrM35ZUb+FERLqUnRalaTIRO5J/V/pziX0wQU1Rq6bE5R/XPZSf02zbV1a5erFIS/neOuVtOa9amU/JgRJwjK1eu1LBhw8p9PTMzU5GRkba35k21b98+R+3GjBkTtN/Rzz9HnEzpumbNGs2YMcNfJbnlryF+lZplWV5vkqxgbdOnT7fcmTt3bsBratWqlduayjJixIig/Q7ZArOlJ1Wx/jmkjnXw8aZWztgLf9h2PtLE+lvfNKtWtaig1/irX/3K9m+1TZs2Xh07KyvL0Xsh2L8Dd1utWrUc/QyNGjUKeq3nt9GjR9vWu2DBgqDX6WRzYtGiRUGv08c/s8eZHNa33N0J9IIsktSkSROP92EddLO1qB2jpXc31A3tqv9iBria8ZG681fJ+mZUhno0jQ9SheesX7/edpWwyjx87fDhwz+ZVrU87du3D0A1zmzYsMG2TWZmZsjNhAnvhWWgp6en287cZLeKlD80btzY430IdHNFuKR//baOkuLczzCXFBepD2+qp3b1gjc06tSpU7YLdVTmQJecXST07NkzAJU4s3LlSts2ycnJyszMDEA1CISwDHQnQ2gqOmTNG95cobN0qrkuqhWjFrXdz2R4XnSkS8/1r+Xnitzzds3tymLx4sW2bQYNGqToaO/m8ve1PXv2OPp8uf766wNQjf9VqVIl2CUEXVgGut2VwokTJ7Rz584AVfM/Za1J7E5eXl6lX8XKZMlV7ed+/7H29WPVIDl4H0p2Pd1btGih+PjgPhoIJiedx1JSUtx2Lgy0mTNn2rb53e9+F/Lzrts9DpKczXdvOiMDPRi32yXPb7lzu91spws9n2f9Ii9XavMFuyv0iIgIXXLJJQGqJvRs3LhRW7ZssW03ZswYx3O/+9uUKVNs2yQlJWnMmDEBqMZ7ubn2c0DUrl07AJWEtrAL9MjISNuOJ8G43R4dHe1odaMf43a72b47VKjcgoovnhIoTp4RV/bb7q+88optm9q1a+vf//53SNx6X7x4sTZu3Gjb7u677w6pOws/d+yY/cqCNWrUULNmzQJQTegKu0Bv3ry5EhIS3LYJRg/3Ro0aeTyr3O7du/1UDUJBUamll7467rh9caml1R5OGetLOTk52rVrl9s2lb1j3MSJEx1N7HTVVVdp9uzZSktLC0BV7j377LOO2k2cOFGjRo0KyV7vTses33LLLX6tI9SFXaA7mbGKIWsIFc8vOqY5W/IctX1t2QkdzQvuxC2svOZeXl6eHnvsMUdte/bsqe+//14PP/ywT2/Bx8XFacCAAXrnnXcc1TJ58mRHn4kREREaN26cli9frj59+vh02usGDRpo1KhRWrx4sVq1auXx/k4/0++77z5ddtllHh/fFGE3U5zdDHF5eXnasWNHgKr5H2+GrHHL3XylZ6Whk/frz91TdO8VNRRX5ZdXP+cXZnn8s+DPALhq1SoNHjy43NcbNGig2rVrV3hZ4rJMnz7d58f0xJEjRzR8+HDbdm+88YZuvPFGXXHFFbZtk5KSNHbsWD3++OP6/PPP9fnnn2vlypXavn27o5ksq1WrpsaNG6tZs2bq0KGDOnfurMzMTMXEnBs98cILL9ge4+zZsxo+fLiWL1/u6DFAx44dNWvWLB08eFAzZ87U4sWLtWnTJm3fvt3RdLL16tVTkyZN1KJFC3Xu3FmdOnX6yedjZKRnnUUl6fPPP9cDDzxg2y42NlaLFy/Wiy++qDfffLPMNQpcLpeaNm2qXr16acaMGUZdWLmc9B4sd2eXy/udvbRmzRq1bdu23NeXLFni6I3ma9ddd5169+7t0T7PPfeco+dbMEONqpHq1yLh3HSu8VE6U3xW3x85t5DLtuyiYJcnSbr66qs1d+5ct2369+/vqPf0eVlZWWGx/Oru3buVkZHhqG3dunW1du1apaamen2+/Px87d27V/n5+crNzVVBQYEiIyOVkJCg+Ph4paam2t6yf+GFFzRq1ChH5xs5cqRefvllr+uVzn3pOXz4sAoLC3X69GkVFBQoNjZWiYmJio+PV7169WzXYW/Xrp3tiIqfi46O1r59+zz+fefk5PzwRcSyLCUlJalx48Y/9Or3ppZAsSzL82cf3kwvd35TgKfCi4uLs0pKStxO/zdu3LigT9nHxhauW2pqqu0Um0899ZRHx3Q69WuwZWVlefRztW7d2srJyQlqzePHj/eo5r///e9BrdeyLKtt27Ze/W0+/vjjIVNLIDbL9Klfzy8Q4U4wergDpjh69KjtLcjKvpTqeevXr1ePHj3Cajnjhx56SKNHjw52GV557rnnjLo97g9hFehOhswEo0McYBK7L8VOli6uLFatWqX27dtr0aJFwS7Fsb/+9a/6zW9+o+PHnY/ACAV5eXkaNmyYiouLg11KyAqrQLfrYXvmzBlHEz8AKJ/dl+LExMRKP973xw4ePKgePXroD3/4g6Px0qFg2rRpuuiii/Tee+85moUtVHz55ZcaOnQooV6OsAp0u1t9GzZsqLTrNQO+4uQuV2UfvvZzlmXp9ddfV0ZGhh566CG/r/VdWlqq2bNnO5oJrjxHjhzR//t//08tW7bU5MmTVVDg3zkQjh49qn/84x+2cx3Y+eijj9StW7egTO8d6sKml3tqaqrthA6vvvqqRo4cGaCKQl/79u318ssvKzY2eKt4hYONGzfqjjvuUF6es/Hipqtbt67tkKpXXnlFd911l6PjffXVV0pPT/dFaX61b98+XX755T45lsvl0hVXXKGBAwfqqquu0sUXX1zhY+7atUvLli3T/PnzNXPmTJ/fMk9MTNTgwYP161//Wj179lRycnKFjldUVKR169bpiy++0Lx58/Tll1/69IIrOjpaf/jDHzRixAhddNFFHu178OBBTZs2TaNHjw7ZuyqWF73cwybQ+/Tpo1mzZrltc/vtt+utt94KUEWh7YYbbtBbb71FmDu0YcMG9e/f3+9XVuHi0KFDqlWr/NXfvv32W67SPVCjRg21bt1arVq1Unp6uurVq6eUlBTFxsYqNjZWpaWlKioqUk5OjgoKCnTy5Ent3r1bu3fvVlZWlrZs2RLQZ94RERFq1KiR2rRpowsvvFDp6elKS0tTjRo1FBERoWrVqunUqVMqKChQQUGBcnNzdeTIEWVlZf1Q9+bNmwN2a7x58+bq0qWL2rRpo/r16ysxMVHVqlVTfn6+SkpKdOTIEe3Zs0cbN27U119/rS1btoT8owZvAj1shq098cQTtkMQ2rVrF/ShBsHeIiIirLFjx1ZwMEfldPToUeuKK64I+r9hKGyzZs1y+7sqLCy0YmJigl4nG5upm2XysDW7q4GioqJKP0lLtWrV9Mknn+jhhx8OdilhKSUlRQsWLHA0W5jp7FYsjI6OdjvBE4DAMybQ169fX6l7Pl5wwQVatmyZ+vfvH+xSwlqVKlU0YcIEvfTSS4qKCruZkX3GyXwOjEcHQktYBHqTJk1sFzeozBPKdOvWTd9++61atmwZ7FKMcdddd2nu3Lkhs651oK1cudK2jZOFkgBfuuCCC/gi6UZYBLqTzjd2twhNdccdd2jevHmqWbNmsEsxTs+ePfX111973IPWBHv27LHthMUHK/wtIiJCHTt21FNPPaUNGzZo586dGjp0aLDLCllhcU/RSaBXthnioqKi9MILL+jOO+/0YC9LBzYs0dnSEr/VFQ4SUuoqKb25o7ZNmjTRihUrdOONN9qOsjDN6tWrdeWVV5b7erNmzZScnKwTJ04EsCqYLi4uTr169VK/fv3Ur18/t6Mt8FNhEeh2U76WlpZq/fr1Aaom+GrUqKGpU6eqe/fujvc5W3RGy8f0Vau4g36sLDwUlVpa1fgmXXLDI47aV69eXTNmzNBDDz2kZ5991s/VhY5Vq1a5DXTp3FX6vHnz3Lb54IMPVLt2bV+WpjNnzqioqEh5eXnav3+/9u3bpz179mjNmjUsSxymmjdvrqefflpXXXUVw229FPKBXqVKFdvetJs2bVJhYWFgCgqyiy++WDNnzlSjRo0c73PqcJa2vTBArapVjt+RnehIl5pmvatlz29S5/snS7If7hkREaFnnnlGrVq10u9///tK8ffm5DFWhw4dbAO9U6dOAV0+NTs7W1999ZVmzJih2bNn6/DhwwE7N7zXvHlzOvVWUMg/Q2/Tpo3ttzUnHXhM0LdvX61YscKjMN+7ZoEOvdxbTQjzX2hZtEpL/6+rSgpOO95n2LBhWrRokc+vOEORk46mThZMCrSUlBQNHDhQb731lvbv36+PP/7Yo7tZQLgK+St0Jx1vIiIiNHDgQP8XE0Tt2rXTo48+qogI59/BNnz6stLWvazEqn4sLMy1qnpEG//aSY3unq7qdRo72qdTp0769ttv9eCDD+rMmTN+rjB4XC6XSkpK3A7fC/XZ4iIjIzVo0CANGjRIixYt0r333lupHs+hcgn5QHcyFOvmm2/WzTffHIBqwsfXr45Q85ML5aoS7EpCX6Nqxcp+ra+O93tBGZde7Wif9PR0vffee36uLPSlpaUpJSVF2dnZwS7FVrdu3bRmzRo999xzevTRRyv1vBUwU8jfck9JSQl2CeGltFjLnrxKF+UsFEtWO5cSJ1Wde6/WfzI+2KWEnXB6j0ZEROjPf/6zFixYUOHFR4BQE/JX6AcOHAh2CWEj//gBbXr+GrVMKP828Mn8Uj0y+6hO5lfOZWa7Nq6q4ZeV/UEeGyVlbPmnlr34nTrf87qcdJZDeL5Hr7jiCi1evFg9evTQ0aNHg10O4BMhH+gLFy7UqFGjgl1GyDu4aaly3/+dmiZY5bbZcaxI172zX9uyiwJYWWj5z6Y8bTpcpOf711JkOfenWp5eoqVP9NJlf/mPIqowfMadb7/9Vrm5uT45Vk5OjqN2iYmJPjlfy5YtNX36dPXo0aNSjFqA+UL+lvusWbO0aUPlndbViS2fvSl9dJvqxJcf5suy8nXla3sqdZif9/Y3J/Wbift0qvBsuW1aRe/TmtGXKe/I3gBWFn6e+ftYnx0rKSnJ0eZyuRQXF6dmzZqpd+/euv/++zVjxgyvvlh07txZ48aN89nPAARTyAd6aWmpRtx6o45lVe6V1MrzzRt/VO1Vzyo+uvzbwx+sydWAN/fq+JnKeZu9LAu3n9aVr+3WnhPld4xqWi1fB166WvvWfRnAysLHlKfv1ifTpwfl3AUFBdq2bZvmzp2rf/zjHxowYMAPw9W++uorj441YsQIdenSxU+VAoET8oEuSYtXbdE/7hqgryc+Ip2t3NOW/uBsiZaP7a/mR2crws2j3ifnZ+uOqQdVVFr+1XtlteVIkXq8tlvf7s0vt03tqmcVOX24Ns6aEMDKQtv+TSu08OHL9fCzr6u0/JscAVdcXKxPP/1UXbp00Y033ujRFftLL72kyMhIP1YH+F/IP0M/79m5u3R1xnvaueNj7YtIl2pcoCrV0xRR3oNQw0Vu/Uwt4sv/wCoosXTn1IP6eP2pAFYVfo7mleqaf+3Va4PraFDramW2iY92KW7dOC3ZvFjRac4n9TFJwakcleYeUuyJ79UiqVDvLTysncdC9/HNv//9b61fv17z589XnTp1bNu3adNG1113nd5///0AVAf4R9gEenGppeEfHdSSuzLUNmqfdGafZO6cHvbiy38p+3Spbnh3v77ZU/6VJ/6noMTSbR8e0I5jKXqge9mr1kW4pDbFq6T9lWsRoF9Ilj7fdlpvfXMy2JXY+u6779SnTx8tX77c0dzg9913H4GOsBZWl7ffHynSE58xxMSd748UqfuruwlzD1mWNGZ+tv7w0UEV83iiXCfzS3XXx4dkhcmvaO3atfrzn//sqG379u1Z4x1hLWyu0M97bdkJ9bkoQZc3Yj7Tn1u4/bRuev+AcgtC6MFmmPlgTa72nCjW+8PqKTmOZ6o/d/+MIzqQG179WF599VWNGDHC0br2Q4YM0YoVK3xeQ0xMjDIyMlS/fn2lpqYqJiZG1apVU15ens6ePasjR45o37592rVrl/Ly8nx+fk9FR0erQYMGqlOnjpKTk1WjRo0fXisuLtbp06d1+vRpHT58WLt373Y85BD+FXaBftaSfj/loD6/s6HqVg+78v1m4jcndf+MIyo5GyaXTiFsWVa+ery6Rx/dXE9NUqKDXU7ImLwqR1PX+WbMeSCVlpZq7Nixevfdd23bDhkyRH/6059kVfAWRFxcnHr27KlrrrlGnTp1UsuWLR11urMsS9u2bdPy5cs1Z84czZ49W6dO+b8fTIMGDdS7d291795dmZmZatKkiVweTDWZm5urHTt2aM2aNVqzZo2WL1+uNWvW6OzZ0Ly4iIyM1N13362kpCRH7RcsWODx6IlgcFXkD9flcgUtPeonVdGEIXX0qwviglVCSDhrSY/NOaqXvjoe7FKMkxQXqfeG1q30d4POWtLLXx3X458d9apXe1ZWlqPlUz0JEE9FR0fr8OHDjj7AW7Zsqe+++86r8zRu3Fj33HOPbr31VlWrVnYnS0/k5eXp/fff19/+9jdlZWVV+Hg/5nK5dM011+iPf/yjunbt6vPf/7FjxzRv3jzdeOONjtoPHDhQn3zyiW27F154oUKTjUVHR+vtt992XNfMmTM1ePBgFRUFthOoZVke/4OE7SXu3pPF6vOvPerSqKr6t0hQqzqxqhbjvEtAfHy8XOWM98o7FfxbXk5YksYuyNbszeFRb7g5mV+qa9/ep79fk6aODcLji2NkVJTi4sruAFZUVKwiD2ZEO3amVF/vztd7q3OUdTy8FzIpKirSZ599puuuu8627eWXX+5xoFevXl2jR4/W3XffrSpVfLciUkJCgoYPH65bb71V48eP12OPPaaCgoIKH7dx48Z66623dMUVV/igyrLVrFlTN9xwg+PgDITo6GhNnTpV/fr1c9Q+WGHuNcuyvN50LlPCcjt58qRVnmDXxsbm7XbLLbeU+3c9ceLEoNSUlZVVbk2BfN/ddNNNjuqYNGmSR8e95JJLrB07djg6dkVt3LjRatSoUYV+D927d7dOnDgRkHoty/m/68CBAx0db/z48V793FWrVrU+//xzx3XPmDHDio6ODsp7RpJleZHJYdXLHQC8tWTJEkft2rVr5/iYV199tZYsWaJGjQIzP0GLFi20YsUKtWnTxqv927Rpo1mzZjl+dmyKxMREzZ8/Xz169HDUfsqUKeF1Zf5fBDqASiErK8tRD/JmzZopIsL+o7Fnz56aOXOm4uIC+zgmNTVV8+bN8/hLRHR0tD766KOA1xtsqampWrBggTp37uyo/fvvv68bb7wx7MJcItABVBKWZWnjRvs1IaKjo2078TVu3FgfffRRhZ6XWxXokJyWlqaPP/5YMTExjve544471LRpU6/PGY7q1KmjL774Qu3bt3fU/v3339dNN92k0tLwXPcibDvFAYCndu3a5WjymAsuuEC7du0q87WIiAi9++67Sk5OdnTO3NxcTZ8+XYsXL9b69eu1Z88eHT169IchXYmJiUpJSVGbNm3Uvn17DRo0SBdeeKHtcdu2bav/+7//06OPPuqojjvvvNNRux+zLEu7d+/W4cOHlZ2drZKSEp06dUrx8fFKSEhQQkKCGjZsqLp163p8bH+rX7++Fi5cqMaNGztqH+5hLkl0iitDsGtjY/N2o1Oc++355593VMv1119f7jFGjhzp6Bi5ubnWfffdZ8XFxXlUo8vlsgYMGGDt2bPH9hwFBQVWgwYNbI/ZpEkTRzVblmXl5+dbL7zwgtWtWzeratWqjmqOj4+32rZta/3hD3+wJk6c+JPanf7cvuwU16RJE2vfvn2Of+bJkydbkZGRQXl/lLdZXmQyV+gAKo0jR444apeSklLmf4+Li3N0Rbxjxw717t1b27Zt86g+SbIsS59++qmWLl2qhQsXqmXLluW2jYmJ0R//+EfbcdlObzkXFhaqW7du+vrrrz0pWadPn9batWu1du1aTZgwQS6XSx07dtSQIUM8Oo4vtGrVSvPnz1etWrUctf/nP/+pO++8s8KTCYUCAr0MAwcO/OF/n39zAaEoPT39Jx/WmZmZQawm9B096mwtiNq1a5f532+99dZyXzsvNzdXffr08SrMfyw7O1t9+/bVhg0bVL169XLb3X777XrooYfcjk932oFu8uTJHod5WSzL0ooVK/wyja47HTp00KxZs8r9QvZzJoW5RKCX6cezFW3ZsoVAR8hKS0tzNLsWznE6T3pCQkKZ//3222+33feBBx7Q1q1bPaqrPHv27NEzzzyjMWPGlNsmISFBvXr10syZM8tt47Rn++bNmz2uMVR06dJF//nPf9x++fkx08Jcope7rXCYvxeV17p163T69OlglxE2KjIUqXnz5rZ3QPbt26e33nrL63OU5Y033rANnauvvtrt6047ejm9sg01V155pT777LNKHeYSgW5r2bJlwS4BKFdpaamWL18e7DLCRnGxsylsy7pC//Wvf22734cffqiSEt+uRnf48GGtW7fObZsOHTq4fd3pamhDhw71yRz0gdS/f3/Nnj3b8V2IcePGGRnmEoFua+nSpcEuAXCLQHfO6d2MqKhfPo3s2bOn7X6LFy/2uCYn7G6FN2/e3O3r27dvd3Se+vXra/bs2Y4W0wkFQ4cO1ccff+x4PoCnn35af/zjH40Mc4lAdys7O9tnz8IAf+FLp3NOrz7LupJ3MiWsv55BHz/ufjXFatWq/WTN8p9bvny54xC7/PLLtW3bNr3xxhtq27atJ2UG1PDhwzVp0qQyv3yV5emnn9ZDDz3k56qCi0B3g9vtCAfLly8P2XWnQ42TNcmlX17J16hRQ/Xq1bPd78CBA17VZccu0CW5fX6cnZ2tRYsWOT5flSpVdPvtt2vNmjXasGGDHn744YDNV+/EqFGjNGHCBEdT9EqVI8wlAt0trnwQDnJzcx1NaYpzyyZ7IyMjw1E7f3VQdPKFze7uw9/+9jevzt2yZUuNHTtWO3bs0IoVK/SnP/3J8exr/jB48GCNGzfOcfvJkydXijCXGLbmFj3cES6WLl2q1q1bB7uMkFe1alVH7Y4dO/aT/1+nTh1H+61du9bTkhyxG/su2d99mD9/vt555x3dfPPNXtfRsWNHdezYUc8++6y+++47TZ8+XR988EFAv1A6uVPyY1dffbVq1aqlw4cP+6mi0EGgl6OwsFCrVq0KdhmAI1999ZVGjBgR7DJCntPZw34+AU1qaqqj/bxd1jRQhg8fruTkZPXv37/Cx2rRooVatGihRx55RKtXr9aECRM0efJknTlzxgeV+k5qaqomTZqkX//618Z2hjuPW+7lWL16tQoLC4NdBuAIPd2dcXp1l52d/ZP/Hx0d7Y9yAq6oqEiDBg3SU0895dN+F5mZmZowYYL27dunxx9/POTWW7/qqqt0//33B7sMvyPQy8HtdoSTXbt26eDBg8EuI+Q5vXW+f//+n/x/p7fqw0FpaakeffRRXXLJJfryyy99euzk5GSNHj1aWVlZuvfeex13QgyEsWPHGj81cqUNdLuhDnSIQ7jhS6g9u/Ha5/18uGpFZpgLVWvXrlW3bt10zTXX+DzYExMTNX78eM2ZM0eJiYk+PXZ57G6nV6lSRR988EG50/qagEAvB0PWEG7svoSG2wxgvpaQkKBmzZrZtjt27NgvOsWF2nNhX5o1a5a6deumSy+9VP/6178czyrnRK9evTR37ly/3+EYP368br31Vtt2TZs21UsvveTXWoKp0ga6u+kZt27d6nhVJiBU2H0JNfnKxIk2bdrI5XLZtiurx3ZlmC9/5cqVGj58uGrXrq3rrrtO06ZNc7uCm1OdOnXSK6+84oMKy/bkk0/qvvvu0zvvvKOpU6fatr/lllt0/fXX+62eYKqUvdxjYmLcXqFzux3haM2aNcrPzy93TuvK/iW1e/fujtqVteTn3r17He07adKkoPWkdjL5jBMFBQWaMmWKpkyZooSEBF1zzTUaMmSI+vbtq5iYGK+Oecstt+jNN9/0+WOhBx98UM8888wP//+OO+5Q586dVbduXbf7TZgwQStWrFBWVpZP6wk6y7K83iRZ4bhdfvnllju///3vg14jG5s328KFC8v9u969e3dQasrKynL7fjvP33V88803juro27fvL/atV6+eo30bNGgQ9L8Bf22JiYnWsGHDrJkzZ1rFxcWOfh8/9tFHH7k9/sCBAz063ogRI8o8zlVXXeVo/2XLlllRUVFB/72Wt1neZLI3O1lhHuiPPvqo23/oiy66KOg1srF5sz311FNu/7YzMjICXlMoBHqLFi0c1VBYWGhVr179F/u7XC4rJyfHdv+BAwcG/W8gEFvdunWtJ5980jp58qSj36tlWdapU6es6Ojoco/pNNBLS0utoUOHuq3vxRdfdHSsMWPGBP13Wd5mEejOtgULFpT7D3zs2DHL5XIFvUY2Nm+23r17u/0Au+WWWwJeUygE+ttvv+2ohpkzZ5Z7jC+++MJ2/3HjxgX9byCQW82aNd1+nv5cmzZtyj2W00CfMGGCbV2xsbHWpk2bbI9VWlpqde3aNei/x7I2y4tMrnSd4qKjo9W5c+dyX1+2bJnxswnBXHaranXr1i1wxYSI1q1ba9iwYY7aTpkypdzXnPStGTx4cEiNvfa3Y8eO6dprr9W+ffsctXcyysBOfn6+bZuCggINHTq0zFXzfiwiIkLvvfee25XqwkmlC/QOHTqU22lIokMcwtvJkye1adOmcl+vbIEeExOjt99+21HInjhxQtOmTSv39RkzZtgeIz09Xb/5zW88qjHcnTp1Su+9956jtoEcOrlmzRo99thjtu3q1aunN954IwAV+V+lC3S7DzTGnyPcuftS2rBhQ8crh4W7yMhITZw40fHsYK+99prb4WkrV6501Nv9b3/7m1EzyznhdJZCd0u8+sOzzz6rJUuW2La79tprdccddwSgIv8i0H+kuLhY3377beCKAfzA7i5TZbhKT0xM1NSpUx2PN87NzdWLL77oto1lWXr11Vdtj9WoUSO9/vrrjsa8myItLc1Ru5MnT/q3kJ8pLS3VTTfdpFOnTtm2HTdunC6++OIAVOU/lSrQ7Z6fr1692tHzGSCUVeZAj42N1W233aaNGzdq4MCBjvcbPXq0o+U1//WvfzkKh6FDh2rSpEler7/uD127dlWXLl0UEeHbj/2oqCgNGTLEUdtDhw759NxOZGVl6a677rJtFxsbqw8++ECxsbEBqMpPvOlJd35TCPQE9GSzG3/+/PPPB71GNjZfbIcOHSr373zXrl0BrcXfvdxr1aplDR482HrttdesEydOODrXj61cudKj8cgPPvig42Pv2rXLGjZsmNvhWp5uLpfLat++vTV27Fjrnnvucbzf448/blmWZR04cMB65ZVXrCFDhlj169evUC0pKSnW1KlTHf8+3J3PaS/38ePHe1XrRx995Oj4L730UkDfH+VtlheZXKlmiuvSpYvb1+kQB1MsXbpUgwYNKvO1jIwMpaenO+6ZHCjjx4+3bRMfH6/o6GglJyerbt26ysjIUM2aNb0+5/Hjx/Xb3/7W7VTQZdV54403qnXr1rZtMzIyNGnSJD3//PP65JNPtHDhQn377bfavXu343OmpqaqefPmuvTSS9WpUyddfvnlP6wa98ILLziu+7w6derozjvv1J133inp3PPvtWvXaufOndq5c6eysrJ0/Phx5eXl6cyZMzpz5oxiYmIUFxenpKQkpaWlqXnz5mrfvr169erl+Ip269atjmfc8wens8jdddddmjdvnmbOnBmgynynUgV6165d3b5OoMMU7gJdOnfbffLkyQGsyN69994b0PPl5+drwIAB2rlzp0f7FRYW6rrrrtPKlSsd31JPTU3V8OHDNXz4cEnn1pLIysrS3r17lZubq/z8fBUWFko61xM8ISFBKSkpatKkid87ktWpU8fxsrIVMXHiRL+fw51jx47p1ltv1WeffWbb9u2331br1q114MCBAFTmQ95c1ltheMs9KirKysvLK/c2y/bt24NeIxubr7ZOnTq5va34xhtvBKwWp7fcA+nEiRPW5ZdfXqGfq0ePHlZBQUFQfw5Pbj+fv+UeDEePHrWSkpLc1ufvW+7nN6ezyC1YsMCKiIgI2nvYYmKZ8rVv397tt2mGq8Ekq1evdrtSlskd4+ysXbtWl156aYUXCvniiy907bXXOuokV9mNGDEi4D3cy/PnP/9Zmzdvtm3Xs2dPPfDAAwGoyHcqTaDbfYBxux0mKSoqcjsEs3HjxkpPTw9gRcF36tQpPfTQQ+rYsaO2b9/uk2POmTNHHTt2dBQQldXDDz/saFnTQHE6i5wkjRkzRh06dAhAVb5BoP+Xr5f1A4KtMg9f+7Hjx4/r6aefVtOmTfX000+rqKjIp8ffvHmz2rVrpyeffNIn64ebIj8/X7feeqv+/ve/B7uUX3A6i1xUVJT+/e9/B3SGu4qoNJ3iFixYUOY6x5J09uxZvmHDOO+///4PHa3K4mTcdbjKycnR3LlzNWPGDE2bNs3vQVtYWKjHHntMr732mh588EHddtttfg+BdevWae7cuY7bZ2dny7Isv094Y1mWpkyZogcffFC7d+/267kq4tlnn1WfPn1sRz81atRIr776quP1AILKmwfvVhh2imNjYwvO5o9OcSUlJdbJkyetw4cPW6tWrbI+/fRT66WXXrJuu+02q02bNkFf57pq1arW0KFDralTp1rHjx/3yc+cn59vLVy40PrLX/5iXXzxxV7VVbt2beu2226z3n//fWvr1q3W2bNnfVKbZVnWhg0brCeeeMJq3LixV7UFqlPcj7eMjAwrNzfX0Xntlmz19WZ5kckuqwIri/13mVEAQDkiIyPVokULtW7dWi1btlTDhg1Vp04dJSUlqXr16j/M3HZ+6NrJkyeVl5enrKwsZWVlac+ePdqyZYs2bdrk0Xh5J+Lj49WyZUtdeOGFqlevntLT01WvXj3VqlVLcXFxiouLU0xMjOLj43+Y5/748eM6fvy49u7dq127dmnt2rX65ptvgjILnMksy/L4VgqBDgBAiPEm0CtNpzgAAExGoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwAIEOAIABCHQAAAxAoAMAYAACHQAAAxDoAAAYgEAHAMAABDoAAAYg0AEAMACBDgCAAQh0AAAMQKADAGAAAh0AAAMQ6AAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBggIoG+hmfVAEAAM7zKlsrGuhZFdwfAAD81G5vdqpooK+u4P4AAOCnVnmzU0UDfVYF9wcAAD8125udXJZleX1Gl8tVVeduDaR4fRAAAHBetqSGlmV5/BzdF53inqvgMQAAwDnPy8tOcRW9QpekGJ17ln6x1wcCAACbJbWTVOhNNvtiHHqhpCGScnxwLAAAKqMcSb/VuUz1iq8mltkk6Rqdu/cPAACcy9a5DN1YkYP4cqa4ryR1lLTeh8cEAMBk63UuO7+q6IF8PfXrTkmXShop6bCPjw0AgCmO6FxWtte57KwwX3SKK0+cpGsl/VrnQr7hf/8bAACVTb7ODfP+VtJcSZ/897+VyZtsrlCgAwCA0MBqawAAGIBABwDAAAQ6AAAGINABADAAgQ4AgAEIdAAADECgAwBgAAIdAAADEOgAABiAQAcAwAAEOgAABiDQAQAwwP8HiLP47VVkJnQAAAAASUVORK5CYII=";
}